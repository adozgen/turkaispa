import Vue from 'vue';
import Vuex from 'vuex';

import { auth } from './auth.module';
import student from "./modules/student";
import shared from "./modules/shared";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    student: student,
    shared: shared
  }
});
